from GrabberWebDriver import *
from GrabberHelper import *


class GrabberException(BaseException):
    pass


class GrabberBase (GrabberWebDriver):
    def get_elements(self, selectors, parent=None, hungry=False):
        if type(selectors) is str:
            selectors = [selectors]

        if not parent:
            parent = self._valid_driver()

        elements = list()
        for selector in selectors:
            elements += parent.\
                find_elements_by_css_selector(selector)

            if not hungry and len(elements):
                break

        return elements

    def get_element(self, selectors, parent=None, hungry=False, index=-1):
        elements = self.get_elements(selectors, parent, hungry)
        return elements.pop(index) if GrabberHelper.is_valid_index(elements, index) else None
