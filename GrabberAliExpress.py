from GrabberBase import *
import time


class GrabberBaseAliExpress (GrabberBase):
    __url_main = 'https://www.aliexpress.com/'
    __url_login = 'https://login.aliexpress.com/'
    __url_orders = 'https://trade.aliexpress.com/orderList.htm'
    __url_tech = 'https://sale.aliexpress.com/__pc/techdiscovery_new.htm'
    __url_flash = 'https://sale.aliexpress.com/flashdeals.htm'
    __url_wish_list = 'https://my.aliexpress.com/wishlist/wish_list_product_list.htm'

    def __init__(self):
        super().__init__()

    def _login(self):
        if not self._valid_driver().\
                current_url.count(self.__url_login):
            self._valid_driver().get(self.__url_login)
        self._valid_driver().switch_to_frame('alibaba-login-box')

        object_user = self.get_element(['#fm-login-id'])
        object_pass = self.get_element(['#fm-login-password'])
        object_submit = self.get_element(['#fm-login-submit'])
        assert object_user and object_pass and object_submit

        object_user.clear()
        object_user.send_keys('anton.stefanovich@gmail.com')

        object_pass.clear()
        object_pass.send_keys('SafetyFirst!')
        object_submit.submit(), time.sleep(7)

        self._valid_driver().\
            switch_to.default_content()

    def _load_page(self, url):
        self._valid_driver().get(url)

        if self.get_element('#login-sub'):
            self._login()
            self._valid_driver().get(url)

    def _get_deals(self, url, containers):
        self._load_page(url)

        self._scroll_element(containers.get('deals'))
        deal_objects = self.get_elements(containers.get('deal'))

        deals = []
        for deal in deal_objects:
            price_curr = self.get_element(containers.get('price').get('now'), deal)
            price_last = self.get_element(containers.get('price').get('last'), deal)
            description = self.get_element(containers.get('description'), deal)
            link = self.get_element(containers.get('link'), deal)

            if False \
                    or not (price_curr and price_curr.text.count('$')) \
                    or not (price_last and price_last.text.count('$')) \
                    or not (link and link.get_attribute('href')) \
                    or not (description and description.text) \
                    or not True:
                continue

            description = description.text
            link = link.get_attribute('href')
            price = GrabberHelper.price_to_number(price_curr.text)
            discount = 100 * (1 - price / GrabberHelper.price_to_number(price_last.text))

            deals += [{
                'description': description,
                'discount': discount,
                'price': price,
                'link': link,
            }]

        return deals

    def _scroll_element(self, css, delta=512):
        element = self.get_element([css])
        current_position = element.location.get('y')
        while current_position < \
                element.size.get('height') + \
                element.location.get('y'):

            self.scroll_page_to(
                element.location.get('y') +
                element.size.get('width') / 2,
                current_position), time.sleep(1)

            current_position += delta

    def get_flash_deals(self):
        return self._get_deals(
            self.__url_flash, {
                'deals': '.deals-container',
                'deal':  '.deals-container .deals-item-outer',

                'link': '.deals-item-inner > a',
                'description': '.item-details h3',
                'price': {
                    'now':  '.current-price',
                    'last': '.original-price del',
                }
            }
        )

    def get_tech_deals(self):
        return self._get_deals(
            self.__url_tech, {
                'deals': '.tech-popular-product',
                'deal':  '.tech-popular-product .item-box',

                'link': '.detail-box .title a',
                'description': '.detail-box .title',
                'price': {
                    'now':  '.detail-box .price',
                    'last': '.detail-box .listprice',
                }
            }
        )

    def get_pick_deals(self):
        return self._get_deals(
            self.__url_tech, {
                'deals': '.top-picks',
                'deal':  '.top-picks .product-inner',

                'link': '.photo a',
                'description': '.text-position-box .p-title',
                'price': {
                    'now':  '.money-now',
                    'last': '.money-before',
                }
            }
        )

    def create_wish_list(self, wish_list_prefix):
        self._load_page(self.__url_wish_list)
        wish_list_name = wish_list_prefix + ' ' + \
            time.strftime('%Y-%m-%d %H:%M:%S')

        new_list_button = self.get_element(['#switchWishListBtn'])
        new_list_button.click(), time.sleep(1)

        new_list_input = self.get_element(['.list-name-input'])
        new_list_input.send_keys(wish_list_name)

        new_list_submit = self.get_element(['.ui-button-primary'])
        new_list_submit.click(), time.sleep(5)

        return wish_list_name

    def push_to_wish_list(self, wish_list_name, products_list):
        for product in products_list:
            self._load_page(product.get('link'))

            add_to_wish_list_button = self.get_element(['#j-add-wishlist-btn'])
            add_to_wish_list_button.click(), time.sleep(1)

            wish_lists = self.get_elements(['.user-wish-list li a'])
            wish_lists.reverse()
            for wish_list in wish_lists:
                if wish_list.text.startswith(wish_list_name):
                    wish_list.click(), time.sleep(3)
                    break

    def get_wish_lists(self):
        self._load_page(self.__url_wish_list)

        wish_lists_info = {}
        wish_lists = self.get_elements(['.left-nav-list dd a'])
        for wish_list in wish_lists:
            wish_list_name = wish_list.get_attribute('title')
            if len(wish_list_name):
                wish_lists_info[wish_list_name] = \
                    wish_list.get_attribute('href')

        return wish_lists_info

    def delete_wish_lists(self, wish_lists):
        for wish_list in wish_lists:
            self.delete_wish_list(
                wish_lists.get(wish_list))

    def delete_wish_list(self, link):
        self._load_page(link)

        drop_down_button = self.get_element(['#editWishListBtns .ui-split-button-trigger'])
        drop_down_button.click(), time.sleep(1)

        delete_button = self.get_element(['#editWishListBtns .js-remove'])
        delete_button.click(), time.sleep(1)

        confirm_button = self.get_element(['.ui-button-primary'])
        confirm_button.click(), time.sleep(3)

    def get_my_orders(self):
        self._load_page(self.__url_orders)
        list_products = self.get_elements(['.baobei-name'])
        assert list_products

        list_info = {}
        for product in list_products:
            product_id = product.get_attribute('productid')
            if product_id not in list_info:
                list_info[product_id] = {
                    'id':   product_id,
                    'url':  product.get_attribute('href'),
                    'name': product.get_attribute('title'),
                }

        return list_info.values()

    def get_product_info(self, url):
        self._load_page(url)
        error_token = self.get_element(
            ['.info-404', '.categories', '.ui-unusual'])

        info = None
        if not error_token:
            self._scroll_element('.main-content')
            info = {
                'name': self.get_product_name(),
                'price': self.get_product_price(),
                'orders': self.get_product_orders(),
                'seller': self.get_product_seller_status(),
                'ratings': self.get_product_ratings(),
                'logistics': self.get_product_logistics(),
                'description': self.get_product_description(),
                'specification': self.get_product_specifics(),
                'packaging': self.get_product_packaging(),
                'images': self.get_product_images(),
            }

        return info

    def get_product_name(self):
        return self.get_element(['.product-name', '#product-name']).text

    def get_product_logistics(self):
        return self.get_element(['.logistics-cost']).text

    def get_product_orders(self):
        return self.get_element(['#j-order-num']).text

    def get_product_price(self):
        return self.get_element(['.p-price-content']).text

    def get_product_ratings(self):
        return self.get_element(['#j-customer-reviews-trigger .percent-num',
                                 '#j-store-header .positive-feedback']).text

    def get_product_seller_status(self):
        return bool(self.get_element(['.top-rated-seller']))

    def get_product_description(self):
        description_object = self.get_element(
            ['.product-description-main', '.product-custom-desc'])

        images = list()
        for description_image in self.get_elements(['img'], description_object):
            images.append(description_image.get_attribute('src'))

        return {
            'text': description_object.text,
            'images': images,
        }

    def get_product_specifics(self):
        return GrabberHelper.unwrap_properties(self.get_element(
            ['.product-property-list', '.product-params']).text)

    def get_product_packaging(self):
        return GrabberHelper.unwrap_properties(self.get_element(
            ['#j-product-desc .pnl-packaging-main', '.pdt-pnl-packaging-snapshot']).text)

    def get_product_images(self):
        thumbs_object = self.get_element(
            ['.image-thumb-list', '.image-nav'])
        assert thumbs_object

        image_objects = self.get_elements(['img'], thumbs_object)
        assert image_objects

        image_sources = []
        for image in image_objects:
            image_source = image.get_attribute('src')
            image_sources.append(
                image_source.replace('_50x50', ''))

        return image_sources

    def scroll_page_to(self, x=0, y=0):
        script = 'window.scrollTo(%d, %d)' % (x, y)
        self._valid_driver().execute_script(script)
