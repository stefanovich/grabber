from GrabberConst import *
import requests
import os


class GrabberHelper:

    @staticmethod
    def save_file(content, path, filename=None, binary=False):
        os.makedirs(path, exist_ok=True)

        if filename and len(filename):
            path += os.sep + filename

        mode = 'wb' if binary else 'w'
        file = open(path, mode)
        file.write(content)
        file.close()

    @staticmethod
    def download_file(url):
        data = requests.get(url)
        return data.content

    @staticmethod
    def file_name_from_url(url):
        assert url and len(url)

        crop_to = 'alicdn.com/'
        crop_index = url.find(crop_to)
        url = url[crop_index + len(crop_to):]

        separator_start = url.find('/')
        separator_end = max(url.rfind('?'), len(url))
        filename = url[separator_start + 1: separator_end]

        return filename.replace('/', ' ')

    @staticmethod
    def product_name_from_url(url):
        product_name = url

        index_name = product_name.rfind('/')
        if index_name > 0:
            product_name = product_name[index_name + 1:]

        index_separator = product_name.rfind('?')
        if index_separator > 0:
            product_name = product_name[:index_separator]

        index_dot = product_name.rfind('.')
        if index_dot > 0:
            product_name = product_name[:index_dot]

        return product_name

    @staticmethod
    def param_value_from_url(url, param, default=None):
        param += '='
        value = default
        param_index = url.find(param)
        separator_index = value.find('&', param_index)

        if param_index > separator_index:
            value = url[param_index + len(param) + 1: separator_index]

        return value

    @staticmethod
    def is_valid_index(ls, index):
        valid_indexes = range(len(ls))
        return (index in valid_indexes) or \
               (index + len(ls)) in valid_indexes

    @staticmethod
    def unwrap_properties(text):
        assert text and len(text)
        return text.strip().replace(':\n', ': ')

    @staticmethod
    def is_string_contain(string, keys):
        for key in keys:
            if string.lower().count(key.lower()):
                return True

        return False

    @staticmethod
    def is_number_between(number, between):
        if number is None:
            return True

        assert len(between) == 2

        return \
            max(between[0], between[1]) \
            >= number >= \
            min(between[0], between[1])

    @staticmethod
    def filter_deals(deals):
        filtered_deals = list()
        for deal in deals:

            filter_limits = True
            filter_keywords = False

            for key in deal:
                value = deal[key]

                if type(value) is str:
                    filter_keywords |= GrabberHelper.is_string_contain(
                        value, GrabberFilter.get('keywords'))

                elif type(value) is int or float and \
                        key in GrabberFilter.get('limits').keys():
                    filter_limits &= GrabberHelper.is_number_between(
                        value, GrabberFilter.get('limits').get(key))

            if filter_limits and filter_keywords:
                filtered_deals += [deal]

        return filtered_deals

    @staticmethod
    def price_to_number(price):
        return float(price[price.find('$') + 1:].replace(',', ''))
