from GrabberAliExpress import *
from GrabberContentManager import *
import argparse


def synchronize_orders(grabber):
    products = grabber.get_my_orders()
    for product_info in products:
        try:
            manager = GrabberContentManager(
                info=product_info, grabber=grabber,
                destination='/home/alien/Documents/GalAnt.Flash')
            manager.save()

        except ContentException:
            print("Skipping product %s", product_info.get('id'))


def flash_deals_to_wish_list(grabber):
    return grabber.get_flash_deals()


def tech_deals_to_wish_list(grabber):
    return grabber.get_tech_deals()


def pick_deals_to_wish_list(grabber):
    return grabber.get_pick_deals()


def delete_wish_lists(grabber, pattern=None):
    wish_lists = grabber.get_wish_lists()
    for key in wish_lists:
        if not pattern or key.lower().count(pattern.lower()):
            grabber.delete_wish_list(wish_lists[key])


def save_deals(grabber, deals, prefix):
    delete_wish_lists(grabber, prefix)

    grabber.push_to_wish_list(
        grabber.create_wish_list(prefix), deals)


def go(action):
    grabber = GrabberBaseAliExpress()
    result = action.get('action')(grabber)

    if result and action.get('prefix'):
        deals = GrabberHelper.filter_deals(result)
        save_deals(grabber, deals, str(action['prefix']))


def main():
    mapper = {
        'all':   {
            'map': ['tech', 'pick', 'flash'], },
        'clear': {
            'action': delete_wish_lists, },
        'sync': {
            'action': synchronize_orders, },
        'tech': {
            'action': tech_deals_to_wish_list,
            'prefix': 'Tech', },
        'pick': {
            'action': pick_deals_to_wish_list,
            'prefix': 'Pick', },
        'flash': {
            'action': flash_deals_to_wish_list,
            'prefix': 'Flash', },
    }

    parser = argparse.ArgumentParser()
    parser.add_argument(
        '--action', type=str, choices=mapper.keys())

    params = parser.parse_args()
    action = mapper.get(params.action)
    actions = action.get('map', [params.action])

    for key in actions:
        go(mapper.get(key))


if __name__ == "__main__":
    main()
