GrabberFilter = {
    'keywords': [
        'USB',
        'Bluetooth',
        'Wi-Fi',
        'WiFi',
        'SD',
        'stereo',
        'speaker',
        'earphone',
        'Android',
        'iPhone',
        'iPad',
        'cord',
        'cable',
        'smart',
        'photo',
        'camera',
        'Diffuser',
        'Auto',
        # '',
    ],

    'limits': {
        'price': [5, 30],
        'discount': [7, 100],
        'progress': [7, 100],
    }
}
