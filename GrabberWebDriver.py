from selenium import webdriver
import pickle
import os


class GrabberWebDriver:
    __cookie = 'cookies.pickle'
    __driver = None
    __fabric = {
        'chrome':  webdriver.Chrome,
        'firefox': webdriver.Firefox,
    }

    def __init__(self, driver=None):
        self.__driver = driver if driver else dict()

    def __del__(self):
        if type(self.__driver) is dict:
            for browser in self.__driver:
                if self.__driver.get(browser):
                    self.__driver.get(browser).close()
                    self.__driver[browser] = None

    def _valid_driver(self, browser='chrome'):
        browser = str(browser).lower()
        if not self.__driver.get(browser) and \
                browser in self.__fabric:
            driver = self.__fabric.get(browser)()

            driver.implicitly_wait(3)
            driver.maximize_window()

            self.__driver[browser] = driver

        return self.__driver.get(browser)

    def _save_cookies(self, filename=__cookie):
        with open(filename, "wb") as file:
            pickle.dump(self._valid_driver().get_cookies(), file)

        file.close()

    def _load_cookies(self, load_after=None, filename=__cookie):
        status_success = False
        if os.path.exists(filename):
            cookies = pickle.load(open(filename, "rb"))
            self._valid_driver().delete_all_cookies()
            for cookie in cookies:
                del cookie['domain']
                self._valid_driver().add_cookie(cookie)

            self._valid_driver().get(
                load_after if load_after
                else self._valid_driver().current_url)

            status_success = True

        return status_success
