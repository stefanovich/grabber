from GrabberHelper import *
import os


class ContentException(BaseException):
    pass


class GrabberContentManager:
    __info = None
    __grabber = None
    __destination = None

    __path = None

    def __init__(self, info, grabber, destination):
        assert info
        assert grabber
        assert destination and len(destination)

        self.__info = info
        self.__grabber = grabber
        self.__destination = destination

    def __valid_path(self, sub_path=str()):
        if not self.__path:
            dir_name_max_length = 64  # ToDo const
            dir_name = (self.__info.get('id') + ' ' + self.__info.get('name'))[:dir_name_max_length]
            self.__path = self.__destination.rstrip(os.sep) + os.sep + dir_name.replace('/', ' ')

        return self.__path + os.sep + sub_path

    def __valid_info(self):
        if not self.__info.get('description'):
            assert self.__grabber
            info_ex = self.__grabber.get_product_info(
                self.__info.get('url'))

            if not info_ex:
                raise ContentException("Can't get full product info")

            self.__info.update(info_ex)

        return self.__info

    def synchronized(self):
        return os.path.exists(self.__valid_path())

    def save(self, force=False):
        if not self.synchronized() or force:
            os.makedirs(self.__valid_path(), exist_ok=True)

            self.__save_text(self.__valid_info().get('description').get('text'), 'description.txt')
            self.__save_text(self.__valid_info().get('specification'), 'specification.txt')
            self.__save_text(self.__valid_info().get('packaging'), 'packaging.txt')
            self.__save_text('Price: ' + self.__valid_info().get('price') + '\n' +
                             'Orders: ' + self.__valid_info().get('orders') + '\n' +
                             # 'Top rated: ' + self.__valid_info().get('seller') + '\n' +
                             'Ratings: ' + self.__valid_info().get('ratings') + '\n' +
                             'Logistics: ' + self.__valid_info().get('logistics') + '\n', 'info.txt')

            self.__save_images(self.__valid_info().get('images'), 'primary')
            self.__save_images(self.__valid_info().get('description').get('images'), 'secondary')

    def __save_text(self, content, filename, sub_path=str()):
        GrabberHelper.save_file(
            content, self.__valid_path(sub_path), filename)

    def __save_images(self, images, sub_path=str()):
        for image_url in images:
            filename = GrabberHelper.\
                file_name_from_url(image_url)

            GrabberHelper.save_file(
                GrabberHelper.download_file(image_url),
                self.__valid_path(sub_path), filename, binary=True)
